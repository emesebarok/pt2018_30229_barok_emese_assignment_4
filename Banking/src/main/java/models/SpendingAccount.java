package models;
/**
 * Model class extending the functionality of the abstract Account class.
 * @author Barok Emese
 *
 */
public class SpendingAccount extends Account{

	private static final long serialVersionUID = -1270810452907855091L;

	/**
	 * The constructor instantiating an account.
	 * @param accID The id of the new account.
	 * @param money The money deposited in the new account.
	 */
	public SpendingAccount(int accID, double money) {
		super(accID, money);
	}

	@Override
	public void depositMoney(double money) {
		setMoney(getMoney() + money);
		setChanged();
		notifyObservers(money);
		
	}
	
	@Override
	public void withdrawMoney(double money) {
		setMoney(getMoney() - money);
		setChanged();
		notifyObservers(money);
	}

}
