package models;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.swing.JOptionPane;

import frame.MainFrame;

import java.util.Set;

public class Bank implements BankProc{
	private Map<Person,Set<Account>> bank;
	public Bank(){
		bank = new HashMap<Person,Set<Account>>();
	}
	
	public void addAccforPerson(Person p,Account assocAcc){
		boolean wellFormed = isWellFormed();
		assert wellFormed : "Bank not well formed";
		assert p!= null: "The person must not be NULL";
		
		int initSize;
		if (bank.containsKey(p)){
			initSize = bank.get(p).size();
			bank.get(p).add(assocAcc);
		}
		else{
			Set<Account> accounts = new HashSet<Account>(); 
			initSize =  0;
			accounts.add(assocAcc);
			bank.put(p, accounts);
		}
		assocAcc.addObserver(p);
		
		int finalSize = bank.get(p).size();		
		assert initSize == finalSize-1: "Account could not be added";
		
		wellFormed = isWellFormed();
		assert wellFormed : "Bank not well formed";
	}
	
	public void depositMoney(double sum,int accID,Person p){
		boolean wellFormed = isWellFormed();
		assert wellFormed : "Bank not well formed";
		
		assert p!= null: "The person must not be NULL";
		assert bank.containsKey(p) : "The person must be from the database";
		assert sum >= 0 : "The sum must be positive.";
		
		double initMoney = 0, finalMoney = 1;
		boolean isSavingAccount = false;
		double interestRate = 0;
		if (bank.containsKey(p)){
			Set<Account> accounts = bank.get(p);
			for (Account a:accounts){
				if (a.getAccID() == accID){
					initMoney = a.getMoney();
					a.depositMoney(sum);
					finalMoney = a.getMoney();
					if (a instanceof SavingAccount) {
						isSavingAccount = true;
						interestRate = ((SavingAccount) a).getInterestRate();
					}
				}
			}
		}
		
		
		if (isSavingAccount) {
			assert initMoney == (finalMoney - sum - sum*interestRate): "The money was not deposited";
		}
		else {
			assert initMoney == (finalMoney - sum) : "The money was not deposited";
		}
		
		wellFormed = isWellFormed();
		assert wellFormed : "Bank not well formed";
	}
	
	public void withdrawMoney(double sum, int accID, Person p, MainFrame frame) {
		boolean wellFormed = isWellFormed();
		assert wellFormed : "Bank not well formed";
		assert p!= null: "The person must not be NULL";
		assert bank.containsKey(p) : "The person must be from the database";
		assert sum >= 0 : "The sum must be positive.";
		
		double initMoney = 0, finalMoney = 1;
		boolean isSavingAccount = false;
		double interestRate = 0;
		if (bank.containsKey(p)){
			Set<Account> accounts = bank.get(p);
			for (Account a:accounts){
				if (a.getAccID() == accID){
					initMoney = a.getMoney();
					try {
						a.withdrawMoney(sum);
					}
					catch(Exception e1) {
						JOptionPane.showMessageDialog(frame, "You cannot withdraw more money than what you have.", "Error", JOptionPane.ERROR_MESSAGE);
					}
					finalMoney = a.getMoney();
					if (a instanceof SavingAccount) {
						isSavingAccount = true;
						interestRate = ((SavingAccount) a).getInterestRate();
					}
				}
			}
		}
		
		if (isSavingAccount) {
			assert initMoney == finalMoney + sum + sum*interestRate*2: "The money was not whitdrawn";
		}
		else {
			assert initMoney == finalMoney + sum : "The money was not whitdrawn";
		}	
		
		wellFormed = isWellFormed();
		assert wellFormed : "Bank not well formed";
	}
	
	public void deleteAccount(int accID, Person p) {
		boolean wellFormed = isWellFormed();
		assert wellFormed : "Bank not well formed";
		
		if (bank.containsKey(p)){
			Set<Account> accounts = bank.get(p);
			for (Account a:accounts){
				if (a.getAccID() == accID){
					accounts.remove(a);
					break;
				}
			}
		}
		
		
		wellFormed = isWellFormed();
		assert wellFormed : "Bank not well formed";
		
	}
	
	public void deletePerson(Person p) {
		boolean wellFormed = isWellFormed();
		assert wellFormed : "Bank not well formed";
		
		bank.remove(p);
		
		wellFormed = isWellFormed();
		assert wellFormed : "Bank not well formed";
		
	}
	
	public List<Person> getAllPersons() {
		boolean wellFormed = isWellFormed();
		assert wellFormed : "Bank not well formed";
		assert !bank.keySet().isEmpty() : "There are no persons in the database";
		
		List<Person> resultList = new ArrayList<Person>();
		for(Person person : bank.keySet()) {
			resultList.add(person);
		}
		
		wellFormed = isWellFormed();
		assert wellFormed : "Bank not well formed";
		return resultList;
	}
	
	public Set<Account> getAllAccountsForPersons(Person p) {
		boolean wellFormed = isWellFormed();
		assert wellFormed : "Bank not well formed";
		assert p != null : "The person cannot be NULL";
		assert bank.containsKey(p) : "The person must be from the database";
		return bank.get(p);
	}

	public boolean isWellFormed(){
		for (Entry<Person,Set<Account>> entry:bank.entrySet()){
			if (entry.getValue() == null || entry.getValue().isEmpty()){
				return false;
			}
		}
		return true;
	}
	
	@Override
	public String toString() {
		return "Bank [bank=" + bank + "]";
	}
	
	public int getMaxPersonID() {
		int id = 0;
		for (Person p: getAllPersons()) {
			if (id < p.getPersonID()) id = p.getPersonID();
		}
		return id;
	}
	
	public int getMaxAccIDForPerson(Person p) {
		int id = 0;
		for (Account acc: getAllAccountsForPersons(p)) {
			if (id < acc.getAccID()) id = acc.getAccID();
		}
		return id;
	}

	public Map<Person, Set<Account>> getBank() {
		return bank;
	}

	public void setBank(Map<Person, Set<Account>> bank) {
		this.bank = bank;
	}
}
