package models;

import java.rmi.RemoteException;

/**
 * Model class extending the functionality of the abstract Account class.
 * @author Barok Emese
 *
 */
public class SavingAccount extends Account{

	
	private static final long serialVersionUID = 682226202882594558L;
	private double interestRate = 0.05;

	/**
	 * The constructor instantiating an account.
	 * @param accID The id of the new account.
	 * @param money The money deposited in the new account.
	 */
	public SavingAccount(int accID, double money) {
		super(accID, money);
	}

	@Override
	public void depositMoney(double money) {
		setMoney(getMoney() + money + money * interestRate);
		setChanged();
		notifyObservers(money);
	}

	@Override
	public void withdrawMoney(double money) throws RemoteException {
		double s = getMoney() - money - money * interestRate * 2;
		if (s < 0) throw new RemoteException();
		setMoney(s);
		setChanged();
		notifyObservers(money);
	}

	/**
	 * Gets the interest rate associated to the account.
	 * @return The interest rate.
	 */
	public double getInterestRate() {
		return interestRate;
	}

	/**
	 * Sets the interest rate associated to the account.
	 * @param interestRate The new interest rate.
	 */
	public void setInterestRate(double interestRate) {
		this.interestRate = interestRate;
	}

}
