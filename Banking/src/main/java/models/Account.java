package models;
import java.io.Serializable;
import java.rmi.RemoteException;
import java.util.Observable;

/**
 * Model class implementing functionality related to an account.
 * @author Barok Emese
 *
 */
public abstract class Account extends Observable implements Serializable{
	
	private static final long serialVersionUID = 1953574556560861002L;
	private int accID;
	private double money;
	
	/**
	 * The constructor instantiating an account.
	 * @param accID The id of the new account.
	 * @param money The money deposited in the new account.
	 */
	public Account(int accID,double money){
		this.accID = accID;
		this.money = money;
	}
	
	/**
	 * Deposits a given sum in the account.
	 * @param money The sum to be deposited.
	 */
	public abstract void depositMoney(double money);
	
	
	/**
	 * Withdraws a given sum from the account.
	 * @param money The sum to be withdrawn.
	 * @throws RemoteException 
	 */
	public abstract void withdrawMoney(double money) throws RemoteException;

	@Override
	public int hashCode() {
		int result;
		result = 37 + accID;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Account other = (Account) obj;
		if (accID != other.accID)
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "Account [accID=" + accID + ", money=" + money + "]";
	}

	/**
	 * Gets the account ID.
	 * @return The account ID.
	 */
	public int getAccID() {
		return accID;
	}

	/**
	 * Set account ID.
	 * @param accID The account ID to be set.
	 */
	public void setAccID(int accID) {
		this.accID = accID;
	}

	/**
	 * Gets the money deposited in the account.
	 * @return The deposited money.
	 */
	public double getMoney() {
		return money;
	}

	/**
	 * Sets the deposited money in the account.
	 * @param money The amount to be set.
	 */
	public void setMoney(double money) {
		this.money = money;
	}
}
