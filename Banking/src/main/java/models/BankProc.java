package models;
import java.util.List;
import java.util.Set;

import frame.MainFrame;

public interface BankProc {
	/**
	 * Adds a new account to the set of accounts mapped by a person.
	 * 
	 * @pre person!= NULL && account!= NULL
	 * @post initSize accounts == finalSize accounts - 1
	 * @param acc The account to be added.
	 * @param p The new owner of the account
	 */
	public void addAccforPerson(Person p, Account acc);
	
	/**
	 * Deposits a given sum in an account of a given person.
	 * 
	 * @pre person != NULL && sum >= 0
	 * @post acc.initMoney == acc.finalMoney - sum 
	 * @param sum The sum to be deposited.
	 * @param accID The id of the account.
	 * @param p The owner of the account.
	 */
	public void depositMoney(double sum,int accID, Person p);
	
	/**
	 * 
	 * Withdraws a given sum from an account of a given person.
	 * 
	 * @pre person != NULL && sum >= 0
	 * @post acc.initMoney == acc.finalMoney - sum 
	 * @param sum The sum to be deposited.
	 * @param accID The id of the account.
	 * @param p The owner of the account.
	 * @param frame The one and only frame in this project.
	 */
	public void withdrawMoney(double sum,int accId,Person p, MainFrame frame);
	
	
	/**
	 * Deletes an account with the given accID from the set of accounts of a person.
	 * 
	 * @pre p != NULL && p.accounts[accID] != NULL
	 * @post accounts initSize = accounts finalSize + 1
	 * @param accID The ID of the account.
	 * @param p The owner of the account.
	 */
	public void deleteAccount(int accID,Person p);
	
	
	/**
	 * 
	 * Deletes a given person from the database.
	 * 
	 * @pre p != NULL && bank contains p
	 * @post bank initPersonNr = bank finalPersonNr + 1
	 * @param p Person to be deleted.
	 */
	public void deletePerson(Person p);
	
	
	/**
	 * 
	 * Returns a list of all the persons in the database.
	 * 
	 * @pre persons getSize() != 0
	 * @post @nochange
	 * @return The list of all persons in the database.
	 */
	public List<Person> getAllPersons();
	
	
	/**
	 * Returns a set of all the accounts of a given person.
	 * 
	 * @pre p != NULL && bank contains p
	 * @post @nochange
	 * @param p The owner of the accounts.
	 * @return The set of the accounts.
	 */
	public Set<Account> getAllAccountsForPersons(Person p);

}
