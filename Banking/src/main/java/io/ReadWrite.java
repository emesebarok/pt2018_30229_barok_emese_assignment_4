package io;


import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import models.Account;
import models.Person;

public class ReadWrite {
	public static void saveBank(Map<Person,Set<Account>> t, String filename){
		try{
			FileOutputStream file = new FileOutputStream (filename);
			ObjectOutputStream obj= new ObjectOutputStream(file);
			obj.writeObject(t);
			obj.close();
			file.close();
			} catch(Exception e){
				System.out.println("err"+e.toString());
				}
		}
	
	public static Map<Person,Set<Account>> readBank(String filename){
		try{
			FileInputStream file= new FileInputStream (filename);
			ObjectInputStream obj= new ObjectInputStream(file);
			@SuppressWarnings("unchecked")
			Map<Person,Set<Account>> t= (HashMap<Person,Set<Account>>)obj.readObject();
			obj.close();
			file.close();
			return t;
		} catch(Exception e){
			System.out.println("err"+e.toString());
			return null;
		}
	}

	public static boolean restoreBank(String filename) {
		Map<Person,Set<Account>> tr = new HashMap<Person,Set<Account>>();
		if (readBank(filename) != null) {
			tr = readBank(filename);
			return true;
		} else return false;
	}
}

