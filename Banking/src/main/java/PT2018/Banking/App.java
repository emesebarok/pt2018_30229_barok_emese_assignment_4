package PT2018.Banking;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Set;

import javax.swing.JOptionPane;

import frame.MainFrame;
import io.ReadWrite;
import models.Account;
import models.Bank;
import models.Person;
import models.SavingAccount;
import models.SpendingAccount;

public class App {

	static MainFrame frame;
	static Bank bank;

	public static void main(String[] args) {
		bank = new Bank();
		//bank.setBank(ReadWrite.readBank("bank.txt"));		
		
		Person p = new Person("Emese", 1);
			SavingAccount a1 = new SavingAccount(1, 100);
			SpendingAccount a2 = new SpendingAccount(2, 200);
			bank.addAccforPerson(p, a1);
			bank.addAccforPerson(p, a2);
		
		ReadWrite.saveBank(bank.getBank(), "bank.txt");

		frame = new MainFrame();

		frame.addListPersonsListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				frame.setHeader("List of all persons");
				frame.buildPersonList(bank.getAllPersons());

			}

		});

		frame.addListAccountsListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				frame.setHeader("List of accounts for given person");
				frame.buildAccountList();

			}

		});

		frame.addListAccountsConfirmListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				int personID = 0;
				try {
					personID = Integer.valueOf(frame.txtPersonID.getText());
				} catch (Exception e) {
					JOptionPane.showMessageDialog(frame, "Incorrect personID format", "Error", JOptionPane.ERROR_MESSAGE);
					return;
				}
				
				for (Person p : bank.getAllPersons()) {
					if (p.getPersonID() == personID) {
						System.out.println("Person found.");
						Set<Account> acc = bank.getAllAccountsForPersons(p);
						frame.buildAccountPanel(acc);
						System.out.println(acc.toString());
						break;
					}
				}
			}

		});

		frame.addAddPersonListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				frame.setHeader("Add Person");
				frame.buildAddPerson();
			}

		});

		frame.addAddPersonConfirmListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {

				String name;
				double money;
				String type;
				try {
					name = frame.txtPersonName.getText();
					money = Double.valueOf(frame.txtStartingMoney.getText());
					type = frame.txtAccType.getText();
				} catch (Exception e) {
					JOptionPane.showMessageDialog(frame, "Incorrect format for some of the data", "Error",
							JOptionPane.ERROR_MESSAGE);
					return;
				}

				int id = bank.getMaxPersonID() + 1;
				Person p = new Person(name, id);

				if (type.equals("spending")) {
					SpendingAccount acc = new SpendingAccount(1, money);
					bank.addAccforPerson(p, acc);
				} else if (type.equals("saving")) {
					SavingAccount acc = new SavingAccount(1, money);
					bank.addAccforPerson(p, acc);
				} else {
					JOptionPane.showMessageDialog(frame, "Incorrect format for some of the data", "Error",
							JOptionPane.ERROR_MESSAGE);
					return;
				}
				
				ReadWrite.saveBank(bank.getBank(), "bank.txt");
			}
		});

		frame.addModifyPersonListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				frame.setHeader("Modify person with given ID");
				frame.buildModifyPerson();
			}
		});

		frame.addModifyPersonConfirmListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				int id;
				String name;

				try {
					id = Integer.valueOf(frame.txtPersonID.getText());
					name = frame.txtPersonName.getText();
				} catch (Exception ex) {
					JOptionPane.showMessageDialog(frame, "Incorrect format for some of the data", "Error",
							JOptionPane.ERROR_MESSAGE);
					return;
				}

				int i;
				for (i = 0; i < bank.getAllPersons().size(); i++) {
					if (bank.getAllPersons().get(i).getPersonID() == id) {
						bank.getAllPersons().get(i).setName(name);
						break;
					}
				}

				if (i == bank.getAllPersons().size()) {
					JOptionPane.showMessageDialog(frame, "Incorrect format for some of the data", "Error",
							JOptionPane.ERROR_MESSAGE);
					return;
				}
				
				ReadWrite.saveBank(bank.getBank(), "bank.txt");
			}
		});

		frame.addDeletePersonListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				frame.setHeader("Delete person with given ID");
				frame.buildDeletePerson();
			}
		});

		frame.addDeletePersonConfirmListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				int id;

				try {
					id = Integer.valueOf(frame.txtPersonID.getText());
				} catch (Exception ex) {
					JOptionPane.showMessageDialog(frame, "Incorrect format for some of the data", "Error",
							JOptionPane.ERROR_MESSAGE);
					return;
				}

				int i;
				for (i = 0; i < bank.getAllPersons().size(); i++) {
					if (bank.getAllPersons().get(i).getPersonID() == id) {
						bank.deletePerson(bank.getAllPersons().get(i));
						break;
					}
				}

				if (i == bank.getAllPersons().size()) {
					JOptionPane.showMessageDialog(frame, "Person does not exist.", "Error", JOptionPane.ERROR_MESSAGE);
					return;
				}
				
				ReadWrite.saveBank(bank.getBank(), "bank.txt");
			}
		});

		frame.addAddAccountListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				frame.setHeader("Add account for given person");
				frame.buildAddAccount();
			}

		});

		frame.addAddAccountConfirmListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				int id;
				double money;
				String type;

				try {
					id = Integer.valueOf(frame.txtPersonID.getText());
					money = Double.valueOf(frame.txtStartingMoney.getText());
					type = frame.txtAccType.getText();
				} catch (Exception ex) {
					JOptionPane.showMessageDialog(frame, "Incorrect format for some of the data", "Error",
							JOptionPane.ERROR_MESSAGE);
					return;
				}
				
				Person p = bank.getAllPersons().get(0);
				int i;
				for (i = 0; i < bank.getAllPersons().size(); i++) {
					if (bank.getAllPersons().get(i).getPersonID() == id) {
						p = bank.getAllPersons().get(i);
						break;
					}
				}
				
				if (i == bank.getAllPersons().size()) {
					JOptionPane.showMessageDialog(frame, "Person does not exist.", "Error", JOptionPane.ERROR_MESSAGE);
					return;
				}
				
				int accID = bank.getMaxAccIDForPerson(p) + 1;
				
				if (type.equals("spending")) {
					System.out.println(accID + "      " + money);
					SpendingAccount acc = new SpendingAccount(accID, money);
					bank.addAccforPerson(p, acc);
				} else if (type.equals("saving")) {
					SavingAccount acc = new SavingAccount(accID, money);
					bank.addAccforPerson(p, acc);
				} else {
					JOptionPane.showMessageDialog(frame, "Incorrect format for some of the data", "Error",
							JOptionPane.ERROR_MESSAGE);
					return;
				}
				
				ReadWrite.saveBank(bank.getBank(), "bank.txt");
			}
		});
		
		frame.addModifyAccountListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				frame.setHeader("Modify an account of a given person");
				frame.buildModifyAccount();			
			}
			
		});
		
		frame.addModifyAccountConfirmListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				int personID, accID;
				double money;
				String type;

				try {
					personID = Integer.valueOf(frame.txtPersonID.getText());
					accID = Integer.valueOf(frame.txtAccID.getText());
					money = Double.valueOf(frame.txtStartingMoney.getText());
					type = frame.txtAccType.getText();
				} catch (Exception ex) {
					JOptionPane.showMessageDialog(frame, "Incorrect format for some of the data", "Error",
							JOptionPane.ERROR_MESSAGE);
					return;
				}
				
				Person p = bank.getAllPersons().get(0);
				int i;
				for (i = 0; i < bank.getAllPersons().size(); i++) {
					if (bank.getAllPersons().get(i).getPersonID() == personID) {
						p = bank.getAllPersons().get(i);
						break;
					}
				}
				
				if (i == bank.getAllPersons().size()) {
					JOptionPane.showMessageDialog(frame, "Person does not exist.", "Error", JOptionPane.ERROR_MESSAGE);
					return;
				}
				
				i = 0;
				Set<Account> accounts = bank.getAllAccountsForPersons(p);
				for (Account a:accounts){
					if (a.getAccID() == accID){
						boolean isSavingAccount = false;
						
						if (a instanceof SavingAccount) {
							isSavingAccount = true;
						}
						
						if (type.equals("spending") && !isSavingAccount) {
							a.setMoney(money);
						} else if (type.equals("saving") && isSavingAccount) {
							a.setMoney(money);
						} else if (type.equals("saving") && !isSavingAccount) {
							SavingAccount newAcc = new SavingAccount(a.getAccID(), money);
							accounts.remove(a);
							accounts.add(newAcc);
						} else if (type.equals("spending") && isSavingAccount) {
							SpendingAccount newAcc = new SpendingAccount(a.getAccID(), money);
							accounts.remove(a);
							accounts.add(newAcc);
						} else {
							JOptionPane.showMessageDialog(frame, "Incorrect format for some of the data", "Error",
									JOptionPane.ERROR_MESSAGE);
							return;
						}	
						break;
					}
					i++;
				}
				
				if (i == accounts.size()) {
					JOptionPane.showMessageDialog(frame, "Account does not exist.", "Error", JOptionPane.ERROR_MESSAGE);
					return;
				}
				
				ReadWrite.saveBank(bank.getBank(), "bank.txt");
			}
		});
		
		frame.addDeleteAccountListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				frame.setHeader("Delete an account of a given person");
				frame.buildDeleteAccount();	
			}			
		});
		
		frame.addDeleteAccountConfirmListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				int personID, accID;

				try {
					personID = Integer.valueOf(frame.txtPersonID.getText());
					accID = Integer.valueOf(frame.txtAccID.getText());
				} catch (Exception ex) {
					JOptionPane.showMessageDialog(frame, "Incorrect format for some of the data lol", "Error",
							JOptionPane.ERROR_MESSAGE);
					return;
				}
				
				Person p = bank.getAllPersons().get(0);
				int i;
				for (i = 0; i < bank.getAllPersons().size(); i++) {
					if (bank.getAllPersons().get(i).getPersonID() == personID) {
						p = bank.getAllPersons().get(i);
						break;
					}
				}
				
				if (i == bank.getAllPersons().size()) {
					JOptionPane.showMessageDialog(frame, "Person does not exist.", "Error", JOptionPane.ERROR_MESSAGE);
					return;
				}
				
				Set<Account> accounts = bank.getAllAccountsForPersons(p);
				i = accounts.size();
				bank.deleteAccount(accID, p);
				accounts = bank.getAllAccountsForPersons(p);
				i -= accounts.size();		
				
				if (i == 0) {
					JOptionPane.showMessageDialog(frame, "Account does not exist.", "Error", JOptionPane.ERROR_MESSAGE);
					return;
				}
				
				ReadWrite.saveBank(bank.getBank(), "bank.txt");
			}		
		});
		
		frame.addDepositListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				frame.setHeader("Deposit money in an account of a given person");
				frame.buildDepositMoney();					
			}
			
		});
		
		frame.addDepositMoneyConfirmListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				int personID, accID;
				double money;

				try {
					personID = Integer.valueOf(frame.txtPersonID.getText());
					accID = Integer.valueOf(frame.txtAccID.getText());
					money = Double.valueOf(frame.txtStartingMoney.getText());
				} catch (Exception ex) {
					JOptionPane.showMessageDialog(frame, "Incorrect format for some of the data lol", "Error",
							JOptionPane.ERROR_MESSAGE);
					return;
				}
				
				Person p = bank.getAllPersons().get(0);
				int i;
				for (i = 0; i < bank.getAllPersons().size(); i++) {
					if (bank.getAllPersons().get(i).getPersonID() == personID) {
						p = bank.getAllPersons().get(i);
						break;
					}
				}
				
				if (i == bank.getAllPersons().size()) {
					JOptionPane.showMessageDialog(frame, "Person does not exist.", "Error", JOptionPane.ERROR_MESSAGE);
					return;
				}
				
				i = 0;
				Set<Account> accounts = bank.getAllAccountsForPersons(p);
				for (Account a:accounts){
					if (a.getAccID() == accID){
						bank.depositMoney(money, accID, p);	
						break;
					}
					i++;
				}
				
				if (i == accounts.size()) {
					JOptionPane.showMessageDialog(frame, "Account does not exist.", "Error", JOptionPane.ERROR_MESSAGE);
					return;
				}	
				
				ReadWrite.saveBank(bank.getBank(), "bank.txt");
			}			
		});
		
		frame.addWithdrawMoneyListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				frame.setHeader("Withdraw money from an account of a given person");
				frame.buildWithdrawMoney();					
			}
			
		});
		
		frame.addWithdrawMoneyConfirmListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				int personID, accID;
				double money;

				try {
					personID = Integer.valueOf(frame.txtPersonID.getText());
					accID = Integer.valueOf(frame.txtAccID.getText());
					money = Double.valueOf(frame.txtStartingMoney.getText());
				} catch (Exception ex) {
					JOptionPane.showMessageDialog(frame, "Incorrect format for some of the data lol", "Error",
							JOptionPane.ERROR_MESSAGE);
					return;
				}
				
				Person p = bank.getAllPersons().get(0);
				int i;
				int s = bank.getAllPersons().size();
				for (i = 0; i < s; i++) {
					if (bank.getAllPersons().get(i).getPersonID() == personID) {
						p = bank.getAllPersons().get(i);
						break;
					}
				}
				
				if (i == bank.getAllPersons().size()) {
					JOptionPane.showMessageDialog(frame, "Person does not exist.", "Error", JOptionPane.ERROR_MESSAGE);
					return;
				}
				
				i = 0;
				Set<Account> accounts = bank.getAllAccountsForPersons(p);
				for (Account a:accounts){
					if (a.getAccID() == accID){
						bank.withdrawMoney(money, accID, p, frame);
						break;
					}
					i++;
				}
				
				if (i == accounts.size()) {
					JOptionPane.showMessageDialog(frame, "Account does not exist.", "Error", JOptionPane.ERROR_MESSAGE);
					return;
				}
				
				ReadWrite.saveBank(bank.getBank(), "bank.txt");
			}			
		});

		System.out.println(bank);

	}

}
