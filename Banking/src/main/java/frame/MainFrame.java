package frame;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.Set;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import models.Account;
import models.Person;
import models.SpendingAccount;

/**
 * GUI class containing all the methods and Swing elements needed for the application.
 * @author Barok Emese
 *
 */
public class MainFrame extends JFrame {

	private static final long serialVersionUID = 3769617140967084864L;
	public JPanel contentPanel = new JPanel();
	JPanel buttonPanel = new JPanel();
	
	private JButton btnListPersons = new JButton("List Persons");
	private JButton btnListAccounts = new JButton("List Accounts");
	private JButton btnAddPerson = new JButton("Add Person");
	private JButton btnModifyPerson = new JButton("Modify Person");
	private JButton btnDeletePerson = new JButton("Delete Person");
	private JButton btnAddAccount = new JButton("Add Account");
	private JButton btnModifyAccount = new JButton("Modify Account");
	private JButton btnDeleteAccount = new JButton("Delete Account");
	private JButton btnDepositMoney = new JButton("Deposit Money");
	private JButton btnWithdrawMoney = new JButton("Withdraw Money");
	
	private JLabel lblHeader = new JLabel(); 
	
	public JTextField txtPersonID = new JTextField();
	private JButton btnAccountList = new JButton("Confirm");
	
	JPanel accountPanel = new JPanel();
	JPanel accListPanel = new JPanel();
	
	public JTextField txtPersonName = new JTextField();
	public JTextField txtStartingMoney = new JTextField();
	public JTextField txtAccType = new JTextField();
	public JTextField txtAccID = new JTextField();
	private JButton btnConfirmPersonAdd = new JButton("Confirm");	
	private JButton btnConfirmPersonModify = new JButton("Confirm");	
	private JButton btnConfirmPersonDelete = new JButton("Confirm");
	private JButton btnConfirmAccountAdd = new JButton("Confirm");
	private JButton btnConfirmAccountModify = new JButton("Confirm");
	private JButton btnConfirmAccountDelete = new JButton("Confirm");
	private JButton btnConfirmWithdrawMoney = new JButton("Confirm");
	private JButton btnConfirmDepositMoney = new JButton("Confirm");
	
	public MainFrame() {
		setSize(900, 500);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
		setLayout(new BorderLayout());
		setTitle("Bank Frame");
		
		buttonPanel.setLayout(new GridLayout(10, 1));
		buttonPanel.add(btnListPersons);
		buttonPanel.add(btnListAccounts);
		buttonPanel.add(btnAddPerson);
		buttonPanel.add(btnModifyPerson);
		buttonPanel.add(btnDeletePerson);
		buttonPanel.add(btnAddAccount);
		buttonPanel.add(btnModifyAccount);
		buttonPanel.add(btnDeleteAccount);
		buttonPanel.add(btnDepositMoney);
		buttonPanel.add(btnWithdrawMoney);
		
		this.add(buttonPanel, BorderLayout.WEST);
		
		lblHeader.setFont(lblHeader.getFont().deriveFont(25f));
		
		contentPanel.setLayout(new BorderLayout());
		contentPanel.add(new JPanel(), BorderLayout.WEST);
		contentPanel.add(lblHeader, BorderLayout.NORTH);
		
		this.add(contentPanel, BorderLayout.CENTER);		
		setVisible(true);
	}
	
	public void buildPersonList(List<Person> list) {
		contentPanel.removeAll();
		contentPanel.revalidate();
		contentPanel.add(lblHeader, BorderLayout.NORTH);
		
		JPanel personList = new JPanel();
		personList.setLayout(new GridLayout(0, 1));
		int l = list.size();
		Object[][]dbi = new Object[l][2];
		
		for(int i = 0; i < l; i++) {
			dbi[i] = new Object[] {list.get(i).getPersonID(), list.get(i).getName()};
		}
		
		TableModel model = new myTableModelP(dbi);
		JTable t = new JTable();
		t.setModel(model);
		t.setEnabled(false);
		t.setAutoCreateRowSorter(true);
		JScrollPane sp = new JScrollPane(t);
		sp.setSize(300, t.getRowHeight());
		
		contentPanel.add(sp, BorderLayout.CENTER);
		
		contentPanel.revalidate();
		contentPanel.repaint();
	}
	String[]columnsp = {"IDPerson", "Name"};
	public class myTableModelP extends DefaultTableModel {
		private static final long serialVersionUID = 1L;
		myTableModelP(Object[][] dbi) {
			super(dbi,columnsp);
			}
	}
	
	public void buildAccountList() {
		contentPanel.removeAll();
		contentPanel.revalidate();
		contentPanel.add(lblHeader, BorderLayout.NORTH);
		
	
		accountPanel.setLayout(new BorderLayout());
		
		JPanel head = new JPanel();
		head.setLayout(new GridLayout(1,4));
		head.add(new JLabel("personID: "));		
		head.add(txtPersonID);
		head.add(btnAccountList);
		head.add(new JPanel());
		accountPanel.add(head, BorderLayout.NORTH);
		
		accListPanel.setLayout(new BorderLayout());
		accListPanel.removeAll();
		accountPanel.add(accListPanel, BorderLayout.CENTER);
		
		contentPanel.add(accountPanel, BorderLayout.CENTER);
		
		accountPanel.revalidate();
		accountPanel.repaint();
		contentPanel.revalidate();
		contentPanel.repaint();
	}
	
	public void buildAccountPanel(Set<Account> acc) {
		int l = acc.size();
		Object[][]dbi = new Object[l][2];
		int i = 0;
		String type;
		for (Account a : acc) {
			
			if (a instanceof SpendingAccount) {
				type = "spending";
			}
			else {
				type = "saving";
			}
			dbi[i] = new Object[] {a.getAccID(), a.getMoney(), type};
			i++;
		}
		
		TableModel model = new myTableModelA(dbi);
		JTable t = new JTable();
		t.setModel(model);
		t.setEnabled(false);
		t.setAutoCreateRowSorter(true);
		JScrollPane sp = new JScrollPane(t);
		sp.setSize(300, t.getRowHeight());
		
		accListPanel.add(sp);
		accListPanel.revalidate();
		accListPanel.repaint();
		accountPanel.revalidate();
		accountPanel.repaint();
		contentPanel.revalidate();
		contentPanel.repaint();
	}
	
	String[]columnsa = {"IDAccount", "Money", "Type"};
	public class myTableModelA extends DefaultTableModel {
		private static final long serialVersionUID = 1L;
		myTableModelA(Object[][] dbi) {
			super(dbi,columnsa);
			}
	}
	
	public void buildAddPerson() {
		contentPanel.removeAll();
		contentPanel.revalidate();
		contentPanel.add(lblHeader, BorderLayout.NORTH);
		
		JPanel addPanel = new JPanel();
		addPanel.setLayout(new GridLayout(6, 2));
		
		addPanel.add(new JLabel("Name: "));
		addPanel.add(txtPersonName);
		addPanel.add(new JLabel("Starting money: "));
		addPanel.add(txtStartingMoney);
		addPanel.add(new JLabel("Account type: "));
		addPanel.add(txtAccType);
		addPanel.add(new JPanel());
		addPanel.add(btnConfirmPersonAdd);
		
		contentPanel.add(addPanel, BorderLayout.CENTER);		
		
		contentPanel.revalidate();
		contentPanel.repaint();
	}
	
	public void buildModifyPerson() {
		contentPanel.removeAll();
		contentPanel.revalidate();
		contentPanel.add(lblHeader, BorderLayout.NORTH);
		
		JPanel modPanel = new JPanel();
		modPanel.setLayout(new GridLayout(4, 2));
		
		modPanel.add(new JLabel("ID: "));
		modPanel.add(txtPersonID);
		modPanel.add(new JLabel("New name: "));
		modPanel.add(txtPersonName);
	
		modPanel.add(new JPanel());
		modPanel.add(btnConfirmPersonModify);
		
		contentPanel.add(modPanel, BorderLayout.CENTER);		
		
		contentPanel.revalidate();
		contentPanel.repaint();
	}
	
	public void buildDeletePerson() {
		contentPanel.removeAll();
		contentPanel.revalidate();
		contentPanel.add(lblHeader, BorderLayout.NORTH);
		
		JPanel deletePanel = new JPanel();
		deletePanel.setLayout(new GridLayout(4, 2));
		
		deletePanel.add(new JLabel("ID: "));
		deletePanel.add(txtPersonID);		
	
		deletePanel.add(new JPanel());
		deletePanel.add(btnConfirmPersonDelete);
		
		deletePanel.add(new JPanel());
		deletePanel.add(new JPanel());
		
		contentPanel.add(deletePanel, BorderLayout.CENTER);		
		
		contentPanel.revalidate();
		contentPanel.repaint();
	}
	
	public void buildAddAccount() {
		contentPanel.removeAll();
		contentPanel.revalidate();
		contentPanel.add(lblHeader, BorderLayout.NORTH);
		
		JPanel addPanel = new JPanel();
		addPanel.setLayout(new GridLayout(6, 2));
		
		addPanel.add(new JLabel("Person ID: "));
		addPanel.add(txtPersonID);
		addPanel.add(new JLabel("Starting money: "));
		addPanel.add(txtStartingMoney);
		addPanel.add(new JLabel("Account type: "));
		addPanel.add(txtAccType);
		addPanel.add(new JPanel());
		addPanel.add(btnConfirmAccountAdd);
		
		contentPanel.add(addPanel, BorderLayout.CENTER);		
		
		contentPanel.revalidate();
		contentPanel.repaint();
	}
	
	public void buildModifyAccount() {
		contentPanel.removeAll();
		contentPanel.revalidate();
		contentPanel.add(lblHeader, BorderLayout.NORTH);
		
		JPanel addPanel = new JPanel();
		addPanel.setLayout(new GridLayout(6, 2));
		
		addPanel.add(new JLabel("Person ID: "));
		addPanel.add(txtPersonID);
		addPanel.add(new JLabel("Account ID: "));
		addPanel.add(txtAccID);
		addPanel.add(new JLabel("New money: "));
		addPanel.add(txtStartingMoney);
		addPanel.add(new JLabel("New account type: "));
		addPanel.add(txtAccType);
		addPanel.add(new JPanel());
		addPanel.add(btnConfirmAccountModify);
		
		contentPanel.add(addPanel, BorderLayout.CENTER);		
		
		contentPanel.revalidate();
		contentPanel.repaint();
	}
	
	public void buildDeleteAccount() {
		contentPanel.removeAll();
		contentPanel.revalidate();
		contentPanel.add(lblHeader, BorderLayout.NORTH);
		
		JPanel addPanel = new JPanel();
		addPanel.setLayout(new GridLayout(6, 2));
		
		addPanel.add(new JLabel("Person ID: "));
		addPanel.add(txtPersonID);
		addPanel.add(new JLabel("Account ID: "));
		addPanel.add(txtAccID);
		addPanel.add(new JPanel());
		addPanel.add(btnConfirmAccountDelete);
		addPanel.add(new JPanel());
		addPanel.add(new JPanel());
		addPanel.add(new JPanel());
		addPanel.add(new JPanel());
		
		contentPanel.add(addPanel, BorderLayout.CENTER);		
		
		contentPanel.revalidate();
		contentPanel.repaint();
	}
	
	public void buildDepositMoney() {
		contentPanel.removeAll();
		contentPanel.revalidate();
		contentPanel.add(lblHeader, BorderLayout.NORTH);
		
		JPanel addPanel = new JPanel();
		addPanel.setLayout(new GridLayout(6, 2));
		
		addPanel.add(new JLabel("Person ID: "));
		addPanel.add(txtPersonID);
		addPanel.add(new JLabel("Account ID: "));
		addPanel.add(txtAccID);
		addPanel.add(new JLabel("Amount: "));
		addPanel.add(txtStartingMoney);
		addPanel.add(new JPanel());
		addPanel.add(btnConfirmDepositMoney);
		
		contentPanel.add(addPanel, BorderLayout.CENTER);		
		
		contentPanel.revalidate();
		contentPanel.repaint();
	}
	
	public void buildWithdrawMoney() {
		contentPanel.removeAll();
		contentPanel.revalidate();
		contentPanel.add(lblHeader, BorderLayout.NORTH);
		
		JPanel addPanel = new JPanel();
		addPanel.setLayout(new GridLayout(6, 2));
		
		addPanel.add(new JLabel("Person ID: "));
		addPanel.add(txtPersonID);
		addPanel.add(new JLabel("Account ID: "));
		addPanel.add(txtAccID);
		addPanel.add(new JLabel("Amount: "));
		addPanel.add(txtStartingMoney);
		addPanel.add(new JPanel());
		addPanel.add(btnConfirmWithdrawMoney);
		
		contentPanel.add(addPanel, BorderLayout.CENTER);		
		
		contentPanel.revalidate();
		contentPanel.repaint();
	}
	
	public void setHeader(String text) {
		lblHeader.setText(text);
	}
	
	public void addDeleteAccountConfirmListener(ActionListener a) {
		btnConfirmAccountDelete.addActionListener(a);
	}
	
	public void addModifyAccountConfirmListener(ActionListener a) {
		btnConfirmAccountModify.addActionListener(a);
	}
	
	public void addDepositMoneyConfirmListener(ActionListener a) {
		btnConfirmDepositMoney.addActionListener(a);
	}
	
	public void addWithdrawMoneyConfirmListener(ActionListener a) {
		btnConfirmWithdrawMoney.addActionListener(a);
	}
	
	public void addAddAccountConfirmListener(ActionListener a) {
		btnConfirmAccountAdd.addActionListener(a);
	}
	
	public void addDeletePersonConfirmListener(ActionListener a) {
		btnConfirmPersonDelete.addActionListener(a);
	}
	
	public void addModifyPersonConfirmListener(ActionListener a) {
		btnConfirmPersonModify.addActionListener(a);
	}
	
	public void addAddPersonConfirmListener(ActionListener a) {
		btnConfirmPersonAdd.addActionListener(a);
	}
	
	public void addListAccountsConfirmListener(ActionListener a) {
		btnAccountList.addActionListener(a);
	}
	
	public void addListPersonsListener(ActionListener a) {
		btnListPersons.addActionListener(a);
	}
	
	public void addListAccountsListener(ActionListener a) {
		btnListAccounts.addActionListener(a);
	}
	
	public void addAddPersonListener(ActionListener a) {
		btnAddPerson.addActionListener(a);
	}
	
	public void addModifyPersonListener(ActionListener a) {
		btnModifyPerson.addActionListener(a);
	}
	
	public void addDeletePersonListener(ActionListener a) {
		btnDeletePerson.addActionListener(a);
	}
	
	public void addAddAccountListener(ActionListener a) {
		btnAddAccount.addActionListener(a);
	}
	
	public void addModifyAccountListener(ActionListener a) {
		btnModifyAccount.addActionListener(a);
	}
	
	public void addDeleteAccountListener(ActionListener a) {
		btnDeleteAccount.addActionListener(a);
	}
	
	public void addDepositListener(ActionListener a) {
		btnDepositMoney.addActionListener(a);
	}
	
	public void addWithdrawMoneyListener(ActionListener a) {
		btnWithdrawMoney.addActionListener(a);
	}
}
